resource "aws_key_pair" "id_rsa_aws" {
   key_name   = "id_rsa_aws"
   public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDisapuzEuxnNIi4ptQTBi8u2Eovn1X8557A6TOQEwHhPYlsD41Yb30A0/mHDElRmKcm0RlwgftNrTKbM9i9m6ijSy8c01sW86VlvnG76Xth3SjmQS0HFSY7EqltTt5vzySeQe9P+UCcLT3n9vKJrpeR75LWhXdyRg6O8yXKRFrCcWM8Vk3U+aHe4JuRSPfowpN27rdVWxEwiMfMNSQnL8T/nt15iRXOCNwqc3rJIDRfiJpti5onmUY1sErs1b9JozQ5bevgbCq9dzG6pTo1I4o3ReXOqb0PTlc9La3mgkVzyt/I4Mpw3ZaUVNWTmgZoZq+AG5KqlCrcniouHZJkYmHSMLGy+o6+7RzhzXAymmRe1yLsp0TVrMQFvNR/2C1z2ac4pTXRhGFB56tg/8KFZllVa2B43kbvkSiLZbZGDp8cvYYYl8Y+Ko0jmkSGcWIYaa4meeA/034648bIDxczu5l7vbwtyaRSpJFhP9ki9GPaSPD06bnbho6ggxnFQxZLS8= perrinepeyrichou@MacBook-Pro-de-Perrine.local"
 }
 resource "aws_instance" "server1" {
   ami           = "ami-08ca3fed11864d6bb"
   instance_type = "t2.micro"
   key_name      = "id_rsa_aws"
 }